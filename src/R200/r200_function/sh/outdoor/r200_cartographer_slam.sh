#r200_cartographer_slam
gnome-terminal --window -e 'bash -c "roscore; exec bash"' \
--tab -e 'bash -c "sleep 3;roslaunch r200_bringup r200_bringup.launch; exec bash"' \
--tab -e 'bash -c "sleep 4;roslaunch amovcar_sensor amovcar_lidar.launch; exec bash"' \
--tab -e 'bash -c "sleep 5;source /home/amov/cartographer_ws/devel_isolated/setup.bash && roslaunch cartographer_ros r200_slam.launch; exec bash"' \
--tab -e 'bash -c "sleep 6;roslaunch r200_function r200_slam_rviz.launch; exec bash"' \

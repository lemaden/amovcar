#r200_cartographer_slam
gnome-terminal --window -e 'bash -c "source /home/amov/amovcar/devel/setup.bash && roslaunch amovcar_sensor amovcar_lidar.launch; exec bash"' \
--tab -e 'bash -c "sleep 3;source /home/amov/cartographer_ws/devel_isolated/setup.bash && roslaunch cartographer_ros r200_slam.launch; exec bash"' \
--tab -e 'bash -c "sleep 4;roslaunch r200_function r200_slam_rviz.launch; exec bash"' \

#r200_simulation_navigation
gnome-terminal --window -e 'bash -c "source /opt/ros/melodic/setup.bash && roscore; exec bash"' \
--tab -e 'bash -c "sleep 1;source /opt/ros/melodic/setup.bash && source ~/amovcar/devel/setup.bash && roslaunch r200_simulation r200_2dlidar_simulation.launch; exec bash"' \
--tab -e 'bash -c "sleep 2;source /opt/ros/melodic/setup.bash && source ~/amovcar/devel/setup.bash && roslaunch r200_simulation simulation_teleop_key.launch; exec bash"' \
--tab -e 'bash -c "sleep 3;source /opt/ros/melodic/setup.bash && source ~/amovcar/devel/setup.bash && roslaunch r200_simulation simulation_navigation.launch; exec bash"' \
--tab -e 'bash -c "sleep 4;source /opt/ros/melodic/setup.bash && source ~/amovcar/devel/setup.bash && roslaunch r200_function r200_navigation_rviz.launch; exec bash"' \

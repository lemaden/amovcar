#r200_simulation_slam
gnome-terminal --window -e 'bash -c "roscore; exec bash"' \
--tab -e 'bash -c "sleep 1;roslaunch r200_simulation r200_2dlidar_simulation.launch; exec bash"' \
--tab -e 'bash -c "sleep 2;roslaunch r200_simulation simulation_teleop_key.launch; exec bash"' \
--tab -e 'bash -c "sleep 3;roslaunch r200_function r200_gmapping.launch; exec bash"' \
--tab -e 'bash -c "sleep 4;roslaunch r200_function r200_slam_rviz.launch; exec bash"' \

#r300_vfh
gnome-terminal --window -e 'bash -c "roscore; exec bash"' \
--tab -e 'bash -c "sleep 3;roslaunch mavros apm.launch; exec bash"' \
--tab -e 'bash -c "sleep 4;roslaunch amovcar_sensor amovcar_lidar.launch; exec bash"' \
--tab -e 'bash -c "sleep 5;roslaunch r300_function r300_vfh.launch; exec bash"' \
--tab -e 'bash -c "sleep 6;roslaunch r300_bringup r300_bringup.launch; exec bash"' \
--tab -e 'bash -c "sleep 6;roslaunch r300_function r300_vel_bridge.launch; exec bash"' \

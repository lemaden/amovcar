# amovcar

#### 介绍
amovcar是阿木实验室推出的自主无人车软件系统,针对阿木实验室旗下的R200以及R300无人车特点以及使用场景,集成二维建图,三维建图,室内外导航,避障,GPS(RTK)定位,键盘控制等功能.

#### 软件架构
amovcar是一个ROS工作空间,包含common,R200,R300三大模块.

common: R200无人车以及R300无人车通用的部分代码文件,主要包含传感器和一些通用功能.

R200: R200无人车代码文件,包含R200无人车的功能代码,配置文件,启动脚本文件,仿真模型,驱动代码等内容.

R300: R300无人车代码文件,包含R300无人车的功能代码,配置文件,启动脚本文件,仿真模型,驱动代码等内容.


#### 安装教程

1. 下载amovcar源代码

	$ git clone  https://gitee.com/amovlab/amovcar.git

2. 下载相关依赖ROS功能包

	$ sudo apt-get install ros-melodic-navigation
	$ sudo apt-get install ros-melodic-gmapping
	$ sudo apt-get install ros-melodic-octomap
	$ sudo apt-get install ros-melodic-velodyne-gazebo-plugins

#### 使用说明

可参考无人车使用wiki()进行操作

#### 参与贡献

阿木实验室

